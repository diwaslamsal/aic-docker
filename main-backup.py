from io import StringIO
import io
import json
from fastapi import FastAPI, UploadFile, File, Form
from fastapi.responses import StreamingResponse, FileResponse
from secrets import token_hex
import numpy as np
import uvicorn
import re
import os
import pandas as pd 
import cv2
import base64
import requests

app = FastAPI()

@app.get("/")
def root():
    return {"Hello":"World"}

@app.post("/upload_tsv")
def upload_tsv(file:UploadFile=File(...)):
    file_ext = file.filename.split(".").pop()
    if file_ext!="tsv":
        return {"Incorrect format."}
    
    file_name = os.path.splitext(file.filename)[0]
    # file_name = token_hex(8) # random hex token for csv file name
    # csv_file_path = f"{file_name}.csv"

    tsv_buffer = file.file.read().decode("utf-8")

    csvStringIO = StringIO(tsv_buffer)
    df = pd.read_csv(csvStringIO, sep="\t", header=None)

    # Saving as a file then returning:
    # df.to_csv(csv_file_path,index=False, header = None)
    # return FileResponse(csv_file_path)
    
    # Returning as a streaming response (does not save the file in the server)
    return StreamingResponse(
        iter([df.to_csv(index=False, header = None)]),
        media_type="text/csv",
        headers={"Content-Disposition": f"attachment; filename={file_name}.csv"} 
    )


@app.post("/draw_bounding_box")
async def draw_bounding_box(image:UploadFile=File(...), x_topleft:str=Form(), y_topleftstr=Form(), widthstr=Form(), heightstr=Form()):

    contents = await image.read()
    if "image" not in image.content_type:
        return {"Error":"Not a valid image"}

    # Done: Check image is valid
    nparr = np.fromstring(contents, np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    # Done: Check image bounds for drawing bounding box
    H,W,_ = img.shape
    if int(x_topleft)>W or int(x_topleft)+int(widthstr)>W or int(y_topleftstr)>H or int(y_topleftstr)+int(heightstr) > H:
        return {"Error":"bounding box beyond the image dimensions"}

    # Done: Draw bounding box
    bbox_img = cv2.rectangle(img,(int(x_topleft),int(y_topleftstr)),(int(x_topleft)+int(widthstr),int(y_topleftstr)+int(heightstr)),(0,255,0),2)
    res, im_png = cv2.imencode(".png", bbox_img)
    return StreamingResponse(io.BytesIO(im_png.tobytes()), media_type="image/png")

@app.post("/convert_my_name")
def convert_my_name(name:str=Form()):
    if name!="Diwas" and name!="diwas":
        return {"Error":"Incorrect name."}
    b64name = "http://backend_base64:5000/convert_str_base64?name="+name
    post_response = requests.post(b64name)
    if post_response .status_code == 200:
        return {"Base64 String of my name":json.loads(post_response.content.decode('utf-8'))}
    else:
        return{"Error"}


# Just for test, Remove in docker
if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", reload=True)
