from fastapi import FastAPI
import uvicorn
import base64

app = FastAPI()

@app.post("/convert_str_base64")
def convert_str_base64(name: str):
    """
    Takes a string and returns it in base64 format. 
    args: 
        name: The string to be converted to base64
    returns: 
        String: The base64 output
    """
    # Accepted answer from https://stackoverflow.com/questions/23164058/how-to-encode-text-to-base64-in-python
    b = base64.b64encode(bytes(name, 'utf-8')) # bytes
    base64_str = b.decode('utf-8') # convert bytes to string
    return {base64_str}

