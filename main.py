from io import StringIO
import io
import json
from fastapi import FastAPI, UploadFile, File, Form
from fastapi.responses import StreamingResponse, FileResponse
from secrets import token_hex
import numpy as np
import uvicorn
import re
import os
import pandas as pd 
import cv2
import base64
import requests

app = FastAPI()

@app.get("/")
def root():
    return {"Hello":"World"}

@app.post("/upload_tsv")
def upload_tsv(file:UploadFile=File(...)):
    """
    Takes a TSV file and returns a CSV file. If the input file not TSV, returns "Incorrect format"
    args: 
        file: A TSV file to be converted to CSV
    returns: 
        StreamingResponse: A CSV file that can be downloaded
    """

    # Check if the file extension is tsv
    file_ext = file.filename.split(".").pop()
    if file_ext!="tsv":
        return {"Incorrect format."}
    
    # Read the file and convert it to buffer
    file_name = os.path.splitext(file.filename)[0]
    tsv_buffer = file.file.read().decode("utf-8")

    # read_csv requires conversion to StringIO
    csvStringIO = StringIO(tsv_buffer)
    df = pd.read_csv(csvStringIO, sep="\t", header=None)

    # Return the file as a streaming response in csv format
    return StreamingResponse(
        iter([df.to_csv(index=False, header = None)]),
        media_type="text/csv",
        headers={"Content-Disposition": f"attachment; filename={file_name}.csv"} 
    )

@app.post("/draw_bounding_box")
async def draw_bounding_box(image:UploadFile=File(...), x_topleft:str=Form(), y_topleftstr=Form(), widthstr=Form(), heightstr=Form()):
    """
    Takes an Image file and bounding box parameters and returns an image with bounding boxes.
    Also checks if the input image is of correct format and whether the bounding box goes outside the image bounds.
    Returns error if the aforementioned things are violated.
    args: 
        image: An image file to draw bounding boxes
        x_topleft: The top left X coordinate of the bounding box
        y_topleftstr: The top left Y coordinate of the bounding box
        widthstr: The width of the bounding box
        heightstr: The height of the bounding box
    returns: 
        StreamingResponse: An Image file with the bounding boxes.
    """

    # Checking if the image is valid
    contents = await image.read()
    if "image" not in image.content_type:
        return {"Error":"Not a valid image"}

    # Decode the image for reading and manipulating with opencv 
    nparr = np.fromstring(contents, np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    # Check image bounds before drawing the bounding box
    H,W,_ = img.shape
    if int(x_topleft)>W or int(x_topleft)+int(widthstr)>W or int(y_topleftstr)>H or int(y_topleftstr)+int(heightstr) > H:
        return {"Error":"bounding box beyond the image dimensions"}

    # Draw bounding box
    bbox_img = cv2.rectangle(img,(int(x_topleft),int(y_topleftstr)),(int(x_topleft)+int(widthstr),int(y_topleftstr)+int(heightstr)),(0,255,0),2)
    res, im_png = cv2.imencode(".png", bbox_img)
    return StreamingResponse(io.BytesIO(im_png.tobytes()), media_type="image/png")

@app.post("/convert_my_name")
def convert_my_name(name:str=Form()):
    """
        Takes a Name in Base64 format if the name is correct. Returns an error message if the name is incorrect. 
        Depends on an API response from the backend_base64 server. Returns an error message if the server provides no response.
    args: 
        file: A name to be converted to base64
    returns: 
        String: Base64 string of the given name
    """

    # Check if name is Diwas or diwas or not
    if name!="Diwas" and name!="diwas":
        return {"Error":"Incorrect name."}
    
    # If name is Diwas, run the backend api and convert it to base64
    b64name = "http://backend_base64:5000/convert_str_base64?name="+name
    post_response = requests.post(b64name)

    # If the backend API is successful in converting the name to base64, return it
    if post_response .status_code == 200:
        return {"Base64 String of my name":json.loads(post_response.content.decode('utf-8'))}
    else:
        return{"Error"}
